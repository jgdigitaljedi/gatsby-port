const path = require('path');
const blogTemplate = path.resolve('./src/templates/layout.tsx');

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions;

  const result = await graphql(`
    {
      allMdx {
        nodes {
          id
          excerpt(pruneLength: 150)
          frontmatter {
            title
            date
            slug
            section
            image
            altText
            description
          }
          internal {
            contentFilePath
          }
        }
      }
    }
  `);

  if (result.errors) {
    reporter.panicOnBuild('Error loading MDX result', result.errors);
  }

  // Create blog post pages.
  const posts = result.data.allMdx.nodes;

  // you'll call `createPage` for each result
  posts.forEach((node) => {
    createPage({
      // As mentioned above you could also query something else like frontmatter.title above and use a helper function
      // like slugify to create a slug
      path: `blog/${node.frontmatter.section}/${node.frontmatter.slug}`,
      // Provide the path to the MDX content file so webpack can pick it up and transform it into JSX
      component: `${blogTemplate}?__contentFilePath=${node.internal.contentFilePath}`,
      // component: blogTemplate,
      // You can use the values in this context in
      // our page layout component
      context: { id: node.id }
    });
  });
};

import React from 'react';
import { Box } from 'theme-ui';
import { MetaProps } from '../util/layout.type';
import { MDXProvider } from '@mdx-js/react';
import { default as LayoutWrapper } from '../components/layout';
import { graphql } from 'gatsby';
import ImageBox from '../components/imageBox';
import BlogTable from '../components/blog/blogTable';
import Breadcrumbs from '../components/blog/breadcrumbs';

type LayoutProps = {
  data: any;
  children: React.ReactNode;
  customMeta?: MetaProps;
};

export const WEBSITE_HOST_URL = 'https://joeyg.me';

const Layout = ({ data, children, customMeta }: LayoutProps): JSX.Element => {
  console.log('data', data);
  return (
    <>
      {/* <Head customMeta={customMeta} /> */}
      <LayoutWrapper>
        <Breadcrumbs section={data.mdx.frontmatter.section} />
        <Box as="main">
          <MDXProvider components={{ ImageBox, BlogTable }}>
            {children}
            {/* <Box>{children}</Box> */}
          </MDXProvider>
        </Box>
      </LayoutWrapper>
    </>
  );
};

export default Layout;

export const query = graphql`
  query ($id: String!) {
    mdx(id: { eq: $id }) {
      frontmatter {
        title
        section
      }
    }
  }
`;

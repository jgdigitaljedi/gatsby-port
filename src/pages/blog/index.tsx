import React, { useMemo } from 'react';
import { Box, Flex, Text } from 'theme-ui';
import { containerStyle } from '../../styles/global.style';
import PostTitle from '../../components/postTitle/postTitle';
// import { MainBlogProps } from '../util/post.type';
import RecentCard from '../../components/blog/recentCard';
import CodingBg from '../../images/codingBgOverlay.png';
import GamingBg from '../../images/gamingBgOverlay.png';
import Layout from '../../components/layout';
// import { getSectionPosts } from '../util/mdxUtils';
import { graphql } from 'gatsby';

// @ts-ignore
const Blog: React.FC = ({ data }) => {
  const mostRecent = useMemo(() => {
    const posts = data.allMdx.nodes;
    const recentGaming = posts.filter((post: any) => post.frontmatter.section === 'gaming');
    const recentCoding = posts.filter((post: any) => post.frontmatter.section === 'coding');
    return { coding: recentCoding[0], gaming: recentGaming[0] };
  }, [data]);

  return (
    <Layout>
      <Box sx={containerStyle}>
        {/* <Head>
          <title>Joey Gauthier | Blog</title>
          <meta name="description" content="Joey's Blog" />
          <meta
            name="keywords"
            content="Joey Gauthier, blog, retro gaming, frontend, React, coding blog"
          />
          <link rel="icon" href="/favicon.ico" />
        </Head> */}
        <Box>
          <PostTitle title="Joey's blogs" />
          <Text as="p" sx={{ mt: 4 }}>
            I haven't had a blog in a while and I decided I might enjoy having an outlet for writing
            once again. It makes the most sense to blog about something you love, so I decided to
            host a gaming/retro game collecting blog alongside a blog about software engeering.
          </Text>
          <Flex
            sx={{
              mt: 4,
              justifyContent: 'center',
              // width: ['100%', '100%', '100%', '100%', 'auto'],
              flex: 1,
              flexDirection: ['column', 'column', 'column', 'column', 'row']
            }}
          >
            {mostRecent.coding && mostRecent.gaming && (
              <>
                <RecentCard
                  entry={mostRecent.coding.frontmatter}
                  sectionLink="/blog/coding"
                  sectionLinkSection="Coding"
                  recentText={true}
                  marginRight={true}
                  sectionImage={CodingBg}
                />
                <RecentCard
                  entry={mostRecent.gaming.frontmatter}
                  sectionLink="/blog/gaming"
                  sectionLinkSection="Gaming"
                  recentText={true}
                  sectionImage={GamingBg}
                />
              </>
            )}
          </Flex>
        </Box>
      </Box>
    </Layout>
  );
};

export const query = graphql`
  {
    allMdx(sort: { fields: [frontmatter___date], order: DESC }) {
      nodes {
        excerpt(pruneLength: 150)
        frontmatter {
          title
          date
          slug
          section
          image
          altText
          description
        }
      }
    }
  }
`;

export default Blog;

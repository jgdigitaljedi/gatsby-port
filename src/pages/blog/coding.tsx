// import { getPostsForSection } from '../../lib/getMdx';
import React from 'react';
// import { BlogPaginateProps, IndexProps } from '../../util/post.type';
import SectionLayout from '../../components/blog/sectionLayout';
import { Box } from 'theme-ui';
import { server } from '../../util/helpers';
import { graphql } from 'gatsby';
import Layout from '../../components/layout';

// @ts-ignore
const CodingIndex: React.FC = ({ data }) => {
  // const paginationHandler = (page: any) => {
  //   if (props.router) {
  //     const currentPath = props.router.pathname;
  //     const currentQuery = props.router.query;
  //     currentQuery.page = page.selected + 1;

  //     props.router.push({
  //       pathname: currentPath,
  //       query: currentQuery
  //     });
  //   }
  // };
  console.log('data', data);

  const topText = `To me, writing code is a lifelong endeavor. I wrote BASIC and C++ long before I ever touched a website. Since becoming a web developer, I’ve used everything from Angular, React, Vue, Node, and more. I plan to keep exploring languages and frameworks, as I’ve had my eyes on Rust (for backend and systems stuff), React Native (for cross compiling to mobile), and Svelte (because I'm curious) for a while now. If I feel like writing about something coding related, you will find it here!`;
  return (
    <Layout>
      <Box>
        {/* <Head>
          <title>Joey Gauthier's coding blog</title>
          <meta name="description" content="Joey Gauthier's coding blog" />
          <link rel="icon" href="/favicon.ico" />
        </Head> */}
        <SectionLayout posts={data.allMdx.nodes} section="coding" topText={topText} />
      </Box>
    </Layout>
  );
};

export const query = graphql`
  {
    allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { section: { eq: "coding" } } }
    ) {
      nodes {
        excerpt(pruneLength: 150)
        frontmatter {
          title
          date
          slug
          section
          image
          altText
          description
        }
      }
    }
  }
`;

// CodingIndex.getInitialProps = async ({ query }: NextPageContext): Promise<BlogPaginateProps> => {
//   const page = query.page || 1; //if page empty we request the first page
//   const response = await fetch(`${server}/api/blog/${page}`);
//   const posts = await response.json();
//   return {
//     totalCount: posts.totalCount,
//     pageCount: posts.pageCount,
//     currentPage: posts.currentPage,
//     perPage: posts.perPage,
//     posts: posts.posts
//   };
// };

export default CodingIndex;

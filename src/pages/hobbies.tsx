import React from 'react';
import { Box, Heading, Paragraph, Text } from 'theme-ui';
import { containerStyle } from '../styles/global.style';
import PostTitle from '../components/postTitle/postTitle';
import Divider from '../components/divider';
import ImageBox from '../components/imageBox';
import gameShelves from '../images/gameShelves.jpg';
import ImageGallery from '../components/imageGallery';
import { vgGalleryPics } from '../util/vgGalleryPics';
import Layout from '../components/layout';

const Hobbies: React.FC = () => {
  return (
    <Layout>
      {/* <Head>
        <title>Joey Gauthier | Hobbies</title>
        <meta name="description" content="Joey's Hobbies" />
        <meta
          name="keywords"
          content="Joey Gauthier, retro, video games, Sega, Nintendo, Atari, 3D printing, disc golf"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head> */}

      <Box as="main" sx={containerStyle}>
        <PostTitle title="Joey's hobbies" />
        <Box>
          <Text>
            I have a small handful of hobbies. Some of them are fairly casual hobbies for me, but
            there is one in particular about which I am very passionate. That hobby consumes a lot
            of space in my house, a fair amount of my time, and too much of my disposable income,
            but I wouldn't have it any other way!
          </Text>
          <Box as="p" sx={{ mt: 4 }}>
            That being said, I thought I would briefly highlight some of these hobbies here while
            also building a place to really show off my main hobby!
          </Box>
        </Box>
        <Divider />
        <Box>
          <Heading as="h2">Retro Video Game Collecting</Heading>
          <ImageBox
            imgPath={gameShelves}
            altText="some of my handhelds and a few of my video games"
            containerStyle={{ mt: 4 }}
          />
          <Box as="p" sx={{ mt: 4 }}>
            I’m an avid retro video game collector. It is something I’ve been doing for many years
            now, but I have really gotten serious about it all within the past 4-5 years. My
            collection began as a result of merely buying video game consoles and games over the
            years and holding onto them. I’ve been a fan of video games since I was a kid, so I just
            kept buying new consoles as they came out. Over the years, it started to look like I was
            a casual collector, as my shelves of consoles and games spanned a couple of decades.
            There were certainly periods when I would trade in old games that I no longer had an
            interest in getting something different, but those days are long behind me now.
          </Box>
          <Box as="p" sx={{ mt: 4 }}>
            Over the past few years, collecting retro games, consoles, and accessories has become a
            bit of an obsession. At first, I was merely visiting the local retro games store and
            making the occasional purchase of new games for the consoles I already owned. This was
            fine for a while until I walked into a store one day and noticed a Sega Genesis for
            sale. Man, I have such fond memories of that console from my childhood! When so many
            kids were getting the Super Nintendo for Christmas, I received a Genesis instead and
            played it well into my teen years. I bought that Genesis and a couple of games to go
            with it. Little did I realize at the time that I had just taken my first big step toward
            officially becoming a collector of retro gaming hardware.
          </Box>
          <Box as="p" sx={{ mt: 4 }}>
            The nostalgic feeling I got once I plugged in that old Genesis and turned it on was
            overwhelming. It reminded me of how much simpler and yet somehow more addicting older
            games were. It wasn’t long after this experience that I began seeking more retro
            consoles and using more avenues to do so. Before I knew it, I was creating apps to scour
            various websites like Craigslist to find good deals on retro games and consoles. I even
            started going to garage sales on weekends and purchasing entire lots of retro games from
            people who had deemed them to be junk. My addiction grew, and before I knew it, I was
            buying every retro console I didn’t yet own any time I saw one in the wild.
          </Box>
          <Box as="p" sx={{ mt: 4 }}>
            I remember asking my parents for a Sega Saturn when it initially came out, but the price
            tag was too much for them in the mid-90s. I always thought the Dreamcast was cool, but I
            was already on my own when it was released and there was no way I would have ever
            coughed up that much money for one back then. There are a lot of these sorts of
            scenarios from over the years. Now, I found myself in a position to buy any and every
            one of the consoles I missed out on and start collecting games for them.
          </Box>
          <Box as="p" sx={{ mt: 4 }}>
            At some point, I realized I wasn’t just chasing nostalgia or falling prey to mere
            consumerism. I had a deep interest in the older hardware and games. It was fascinating
            to me that such underpowered old hardware could perform so well because it did one thing
            and one thing only. For example, did you know that the Nintendo 64’s processor is only
            clocked at 93.75 MHz? I thought it was incredible that less than 100 MHz of speed could
            provide such a fluid 3D experience. The more I dug into the facts and history of these
            machines, the more interested I became in owning more of them.
          </Box>
          <Box as="p" sx={{ mt: 4 }}>
            Fast forward to today and I’m the proud owner of over 900 physical games and over 50
            consoles. I’ve got everything from a Game.com to an Atari Jaguar to a Nintendo Virtual
            Boy, and everything in between! There are shelves of the original console and game boxes
            on the walls of my home office. Retro games collecting has become a huge part of who I
            am, so much so that I’ve even learned a bit about electronic circuits and have begun
            modifying, repairing, and refurbishing consoles and games. I bought an Atari 5200 in a
            completely broken state, got it working, and modified the power circuit to take the
            power adapter directly instead of using the problematic power and video box Atari
            created for it. Perhaps one of my favorite modified consoles is my original Game Boy
            Advance, which I moved to an all-aluminum shell with a lithium-ion battery, a USB-C
            charging port, and back-lit IPS screen. There are several stories like this, and I’m
            sure there will be much more of these sorts of things in my future!
          </Box>
          <Box as="p" sx={{ mt: 4 }}>
            I could rattle on about my retro games collection and go deep into the history of video
            games, but I’ll spare you the boredom. Maybe I’ll make a post about it on my blog one
            day? Anyway, below is a gallery of some of my retro games collection. Enjoy!
          </Box>
        </Box>
        <Box>
          <ImageGallery images={vgGalleryPics} />
        </Box>
        <Divider />
        <Box>
          <Heading as="h2">Other hobbies</Heading>
          <Paragraph sx={{ mt: 4 }}>
            Retro video game collecting is somethingon which I've spent a fair amount of time,
            energy, and money. There has to be more to life than that, though. In addition, I also
            spend a fair amount of time:
          </Paragraph>
          <Box as="ul">
            <Box as="li" sx={{ mt: 4 }}>
              <Text sx={{ fontWeight: 'bold' }}>Weight lifting: </Text>
              <Text>
                Anyone who works at a desk needs an activity to get them on their feet, moving, and
                exerting themselves. I've been lifting weights in a garage gym I helped my buddy to
                build for several years now. I enjoy it, believe it or not, and I've achieved some
                of my personal goals such as being able to bench press 100 pounds more than my
                weight! This is something I do 3-4 days a week every week, and that has been the
                case for about 6 years now.
              </Text>
            </Box>
            <Box as="li" sx={{ mt: 2 }}>
              <Text sx={{ fontWeight: 'bold' }}>Drumming: </Text>
              <Text>
                I drummed for a band once upon a time. I was never very good and didn’t even own a
                drum kit, but drummers were hard to find and the guitar player in that band owned a
                drum kit he let me use. A few years back, I bought an electric drum kit and started
                jamming to playlists as another form of exercise. I still try to drum a few days a
                week for 45 minutes or so at a time just to get in a little more sweat. I also find
                drumming an excellent source of stress relief.
              </Text>
            </Box>
            <Box as="li" sx={{ mt: 2 }}>
              <Text sx={{ fontWeight: 'bold' }}>3D Printing: </Text>
              <Text>
                Like many folks, I got an entry-level 3D printer a few years back because I was
                curious about how I could use it. I’ll tell you right now that if you’ve never owned
                a 3D printer, they can be addicting! Before I knew it, I was printing all kinds of
                things for around the house, especially for my video game collection. Finding cool
                things to print and downloading the STL files is great, but I eventually taught
                myself how to design custom 3D models and have printed several of my own creations
                now. This has become something I enjoy so much that I’ve since purchased a better
                printer and have spent a bit of time modifying it to decrease print time and
                increase print quality.
              </Text>
            </Box>
            <Box as="li" sx={{ mt: 2 }}>
              <Text sx={{ fontWeight: 'bold' }}>PC Gaming: </Text>
              <Text>
                Since I am a retro video games collector, I spend some of my time playing older
                games on original hardware. I was always a console guy that was also curious about
                PC gaming. Finally, I built myself a gaming PC a couple of years ago and it has
                since become the primary way I play newer games. In the rare event that I have free
                time on the weekend, you’re likely to find me in my game room/office sitting in
                front of my gaming PC with my headset on.
              </Text>
            </Box>
            <Box as="li" sx={{ mt: 2 }}>
              <Text sx={{ fontWeight: 'bold' }}>Playing guitar: </Text>
              <Text>
                Over the past 25+ years, I have honestly spent thousands of hours with a guitar in
                my hands. I’ve played in several bands and recorded guitar for several albums at
                this point. These days, I’m so busy with work, side hustles, being a father, and
                other activities that I rarely play the guitar. It does still happen, but not nearly
                as often as I would prefer.
              </Text>
            </Box>
            <Box as="li" sx={{ mt: 2 }}>
              <Text sx={{ fontWeight: 'bold' }}>Disc golf, ping pong, and tennis: </Text>
              <Text>
                I’m combining disc golf, ping-pong, and tennis because they are really the only
                sports I still play. I actually play ping-pong with a group that meets every
                Thursday night. It started as a casual activity, but it quickly became competitive
                and is a ton of fun! Tennis and disc golf are both things I play with family and
                friends when the mood strikes and the weather permits.
              </Text>
            </Box>
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default Hobbies;

import React from 'react';
import { Box } from 'theme-ui';
import PostTitle from '../components/postTitle/postTitle';
import { containerStyle } from '../styles/global.style';
import Me8Bit from '../images/me_8bit_scanlines.jpg';
import ImageBox from '../components/imageBox';
import Layout from '../components/layout';

const Home: React.FC = () => {
  return (
    <Layout>
      <Box>
        {/* <Head>
          <title>Joey Gauthier</title>
          <meta name="description" content="Joey Gauthier's portfolio" />
          <link rel="icon" href="/favicon.ico" />
        </Head> */}

        <Box as="main" sx={containerStyle}>
          <PostTitle title="Joey Gauthier" />
          <ImageBox
            altText="pixelated image of Joey with scanlines"
            imgPath={Me8Bit}
            containerStyle={{ mt: 4, maxWidth: '48rem' }}
          />
          <Box sx={{ mt: 4, display: 'flex', justifyContent: 'center' }}>
            <Box as="p">
              Hi! I'm Joey and I'm a software engineer. Welcome to my super simple portfolio and
              blog!
            </Box>
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default Home;

import { ThemeUIStyleObject } from 'theme-ui';
import theme from '../../../gatsby-plugin-theme-ui';

export const sectionLinkStyle = (white?: boolean): ThemeUIStyleObject => {
  return {
    textDecoration: 'none',
    borderBottomWidth: '1px',
    borderBottomStyle: 'solid',
    borderBottomColor: 'inherit',
    fontSize: '1.1rem',
    cursor: 'pointer',
    fontWeight: white ? 'bold' : 'normal',
    color: white ? 'text' : 'primary',
    '&:visited': {
      color: white ? 'text' : 'primary'
    }
  };
};

export const sectionImageHeadStyle = (sectionImage: any): ThemeUIStyleObject => {
  return {
    background: `rgba(0, 0, 0, 0.4) url(${sectionImage})`,
    px: 4,
    pt: 4,
    backgroundPosition: 'center',
    backgroundBlendMode: 'darken',
    borderTopRightRadius: 'md',
    borderTopLeftRadius: 'md'
  };
};

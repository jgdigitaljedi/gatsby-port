import { Link as UILink } from 'theme-ui';
import React from 'react';
import { Box, Card, Heading, Text } from 'theme-ui';
import { PostType } from '../../../util/post.type';
import Divider from '../../divider';
import { sectionImageHeadStyle, sectionLinkStyle } from './recentCard.style';
import { FaLink } from 'react-icons/fa';
import { formatDate } from '../../../util/helpers';
import ImageBox from '../../imageBox';
import { Link } from 'gatsby';

interface RecentCardProps {
  entry: PostType;
  sectionLink?: string;
  sectionLinkSection?: string;
  recentText?: boolean;
  marginRight?: boolean;
  sectionImage?: any;
  staticImg?: boolean;
}

const RecentCard: React.FC<RecentCardProps> = ({
  entry,
  sectionLink,
  sectionLinkSection,
  recentText,
  marginRight,
  sectionImage,
  staticImg
}) => {
  if (entry) {
    return (
      <Card variant="recent" sx={{ mr: marginRight ? [0, 0, 0, 0, 4] : 0 }}>
        {sectionLink && (
          <>
            <Box sx={sectionImageHeadStyle(sectionImage)}>
              <UILink href={sectionLink}>
                {/* <UILink type="normal" sx={sectionLinkStyle(true)}> */}
                <Text sx={{ color: 'text' }}>{`> `}</Text>
                {`${sectionLinkSection} blog landing page `}
                <FaLink />
                {/* </UILink> */}
              </UILink>
              <Divider />
            </Box>
          </>
        )}
        <Box sx={{ p: 4, pt: 2 }}>
          {recentText && sectionLinkSection && (
            <>
              {/* <Link
                to={`/blog/${sectionLinkSection.toLocaleLowerCase()}/${entry.slug}`}
                // href="/blog/[section]/[slug]"
              > */}
              <UILink
                type="normal"
                sx={sectionLinkStyle()}
                href={`/blog/${sectionLinkSection.toLocaleLowerCase()}/${entry.slug}`}
              >
                <Text sx={{ color: 'text' }}>{`> `}</Text>
                {`Most recent ${sectionLinkSection} post `}
                <FaLink />
              </UILink>
              {/* </Link> */}
              <Box sx={{ mt: 2 }}>
                <Text sx={{ fontweight: '700', fontSize: '1.1rem' }}>{entry.title}</Text>
              </Box>
            </>
          )}
          {!recentText && sectionLinkSection && (
            <Box sx={{ mt: '1.5rem', cursor: 'pointer' }}>
              <UILink
                href={`/blog/${sectionLinkSection.toLocaleLowerCase()}/${entry.slug}`}
                // href="/blog/[section]/[slug]"
                // passHref
                aria-label={entry.altText || 'Go to post'}
              >
                <Box sx={{ mb: 2 }}>
                  <ImageBox
                    imgPath={entry.image}
                    altText={entry.altText || ''}
                    noBorder={true}
                    staticImg={staticImg}
                  />
                </Box>
              </UILink>
              {/* <Link
                to={`/blog/${sectionLinkSection.toLocaleLowerCase()}/${entry.slug}`}
                // href="/blog/[section]/[slug]"
                // passHref
              > */}
              <UILink
                type="normal"
                href={`/blog/${sectionLinkSection.toLocaleLowerCase()}/${entry.slug}`}
              >
                {entry.title}
              </UILink>
              {/* </Link> */}
            </Box>
          )}
          <Box sx={{ mt: 2 }}>
            <Text as="p">{entry.description}</Text>
            <Box sx={{ mt: 2 }}>
              <Text>Date posted: </Text>
              <Text sx={{ fontStyle: 'italic' }}>{entry.date}</Text>
            </Box>
          </Box>
        </Box>
      </Card>
    );
  }
  return <></>;
};

export default RecentCard;

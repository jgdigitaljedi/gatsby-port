import { Link } from 'gatsby';
import React from 'react';
import { FaAngleDoubleLeft } from 'react-icons/fa';
import { Box, Flex, Text } from 'theme-ui';
import { Section } from '../../../util/post.type';

interface BreadcrumbsProps {
  section: Section;
}

const Breadcrumbs: React.FC<BreadcrumbsProps> = ({ section }) => {
  return (
    <Link to={`/blog/${section}`}>
      <Flex
        sx={{
          cursor: 'pointer',
          '&:hover': { color: 'primary', textDecoration: 'underline' }
        }}
      >
        <Flex sx={{ backgroundColor: 'bgLighter', p: 2, boxShadow: 'small', alignItems: 'center' }}>
          <FaAngleDoubleLeft />
          <Text sx={{ ml: 2 }}>{`Back to ${section} section`}</Text>
        </Flex>
      </Flex>
    </Link>
  );
};

export default Breadcrumbs;

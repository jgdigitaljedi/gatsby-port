import { ThemeUIStyleObject } from 'theme-ui';

export const linkIconStyle: ThemeUIStyleObject = {
  color: 'primary',
  cursor: 'pointer',
  textAlign: 'center'
};

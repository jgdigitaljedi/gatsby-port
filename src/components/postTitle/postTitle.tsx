import React from 'react';
import { Box, Text } from 'theme-ui';
import { postTitleStyle } from './postsTitle.style';

interface PostTitleProps {
  title: string;
  top?: boolean;
}

const PostTitle: React.FC<PostTitleProps> = ({ title, top }) => {
  return (
    <Box as="h1" sx={{ ...postTitleStyle, mt: top ? 4 : 'auto' }}>
      <Text sx={{ color: 'white', mr: 2, display: ['none', 'none', 'none', 'block'] }}>{'>'}</Text>
      {title}
      <Text
        sx={{
          color: 'white',
          fontSize: '3.5rem',
          ml: 1,
          mb: 1,
          display: ['none', 'none', 'none', 'block']
        }}
      >
        &#x25AE;
      </Text>
    </Box>
  );
};

export default PostTitle;

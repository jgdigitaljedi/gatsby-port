import React, { useMemo } from 'react';
import { Box, Image, ThemeUIStyleObject } from 'theme-ui';
import { imageStyle } from '../../styles/global.style';
import ImagePlaceholder from '../../images/placeholder.jpg';
import { GatsbyImage, getImage, IGatsbyImageData, StaticImage } from 'gatsby-plugin-image';
import { graphql, useStaticQuery } from 'gatsby';

interface ImageBoxProps {
  imgPath: any;
  altText: string;
  maxiWidth?: string;
  eager?: true;
  containerStyle?: ThemeUIStyleObject;
  noBorder?: boolean;
  staticImg?: boolean;
}

const ImageBox: React.FC<ImageBoxProps> = ({
  imgPath,
  altText,
  maxiWidth,
  eager,
  containerStyle,
  noBorder,
  staticImg
}) => {
  const maxWidthCalc = maxiWidth || '40rem';
  const imageRes = useMemo(() => {
    if (imgPath) {
      return imgPath;
    } else if (staticImg && !imgPath) {
      return '/images/placeholder.jpg';
    } else {
      return ImagePlaceholder;
    }
  }, [imgPath, staticImg]);
  console.log('imgPath', imgPath);
  return (
    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
      <Box sx={{ ...imageStyle(noBorder), maxWidth: maxWidthCalc, ...(containerStyle || {}) }}>
        {!staticImg && <Image src={imageRes} alt={altText} />}
        {staticImg && <GatsbyImage image={getImage(imageRes) as IGatsbyImageData} alt={altText} />}
      </Box>
    </Box>
  );
};

export default ImageBox;

import React, { Ref, useEffect, useRef } from 'react';
import { Box, NavLink } from 'theme-ui';
import { NavItems, navItems } from '../../util/constants';

interface MenuProps {
  clickCb: () => void;
  buttonRef: Ref<HTMLButtonElement>;
}

const Menu: React.FC<MenuProps> = ({ clickCb, buttonRef }) => {
  const menuRef = useRef(null);

  const handleClickOutside = (e: MouseEvent) => {
    // @ts-ignore
    if (!menuRef?.current.contains(e.target) && !buttonRef?.current.contains(e.target)) {
      clickCb();
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => document.removeEventListener('mousedown', handleClickOutside);
  });

  return (
    <Box
      sx={{
        backgroundColor: 'bgLighter',
        p: 2,
        boxShadow: 'card',
        position: 'absolute',

        right: 0,
        top: '2.5rem',
        zIndex: 10
      }}
      ref={menuRef}
      role="navigation"
    >
      <Box
        sx={{
          p: 2,
          borderWidth: '4px',
          borderStyle: 'solid',
          borderColor: 'primary',
          display: 'flex',
          flexDirection: 'column'
        }}
      >
        {navItems.map((item: NavItems) => {
          return (
            <NavLink sx={{ pb: 3 }} href={item.link} onClick={() => clickCb()} key={item.title}>
              {'> ' + item.title}
            </NavLink>
          );
        })}
      </Box>
    </Box>
  );
};

export default Menu;

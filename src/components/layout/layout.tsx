import React from 'react';
import { Box, Flex, ThemeProvider } from 'theme-ui';
import theme from '../../gatsby-plugin-theme-ui';
import '../../../static/fonts.css';
import Footer from '../footer';
import Header from '../header';
import ScrollToTop from '../scrollToTop';

const Layout: React.FC<any> = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      <Flex sx={{ minHeight: '100vh', height: '100%', flexDirection: 'column', flex: '1 0 auto' }}>
        <Box
          sx={{
            p: [3, 3, 4],
            height: '100%',
            flex: 1
          }}
        >
          <Header />
          {children}
          <ScrollToTop />
        </Box>
        <Footer />
      </Flex>
    </ThemeProvider>
  );
};

export default Layout;

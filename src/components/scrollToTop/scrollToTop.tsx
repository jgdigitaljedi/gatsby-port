import React, { useEffect, useState } from 'react';
import { Button } from 'theme-ui';
import { FaArrowUp } from 'react-icons/fa';
import { keyframes } from '@emotion/react';
import theme from '../../gatsby-plugin-theme-ui';

const ScrollToTop: React.FC = () => {
  const [visible, setVisible] = useState<boolean>(false);
  const fadeIn = keyframes({ from: { opacity: 0 }, to: { opacity: 1 } });

  const toggleVisible = () => {
    const scrolled = document.documentElement.scrollTop;
    if (scrolled > 300) {
      setVisible(true);
    } else if (scrolled <= 300) {
      setVisible(false);
    }
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  };

  useEffect(() => {
    window.addEventListener('scroll', toggleVisible);
    return () => window.removeEventListener('scroll', toggleVisible);
  });

  if (visible) {
    return (
      <Button
        sx={{
          zIndex: 10,
          right: '.5rem',
          bottom: '1rem',
          position: 'fixed',
          boxShadow: 'card',
          animation: `${fadeIn} .5s backwards`
        }}
        onClick={scrollToTop}
        aria-label="Scroll to top"
      >
        <FaArrowUp color={(theme.colors?.background || '#fff') as string} />
      </Button>
    );
  }
  return <></>;
};

export default ScrollToTop;

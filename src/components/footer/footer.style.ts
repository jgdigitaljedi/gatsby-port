import { ThemeUIStyleObject } from 'theme-ui';

export const footContainerStyle: ThemeUIStyleObject = {
  backgroundColor: 'bgLighter',
  minHeight: '6rem',
  borderTopColor: 'gray',
  borderTopStyle: 'solid',
  borderTopWidth: '1px',
  px: [3, 3, 3, 3, 4],
  py: 4,
  justifyContent: 'center',
  bottom: 0,
  position: 'static',
  width: '100%',
  flexShrink: 0
};

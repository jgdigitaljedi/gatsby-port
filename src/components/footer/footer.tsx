import React from 'react';
import { Box, Flex, Text } from 'theme-ui';
import { footContainerStyle } from './footer.style';
import { Link as UILink } from 'theme-ui';
import { Link } from 'gatsby';

const Footer: React.FC = () => {
  return (
    <Flex sx={footContainerStyle} as="footer">
      <Box sx={{ textAlign: 'center' }}>
        <Text as="p">© Joey Gauthier 2022</Text>
        <Link to="/privacy">
          <UILink sx={{ cursor: 'pointer' }}>Privacy policy</UILink>
        </Link>
      </Box>
    </Flex>
  );
};

export default Footer;

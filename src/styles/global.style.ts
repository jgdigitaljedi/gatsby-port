import { ThemeUIStyleObject } from 'theme-ui';
import theme from '../gatsby-plugin-theme-ui';

export const containerStyle: ThemeUIStyleObject = {
  mt: '2.5rem'
};

export const imageStyle = (noBorder?: boolean): ThemeUIStyleObject => {
  return {
    width: '100%',
    // @ts-ignore
    span: {
      position: 'relative !important'
    },
    // @ts-ignore
    img: {
      objectFit: 'contain',
      width: '100% !important',
      position: 'relative !important',
      height: 'unset !important',
      borderStyle: noBorder ? undefined : 'solid !important',
      borderWidth: noBorder ? undefined : '1.25rem !important',
      borderColor: noBorder ? undefined : `${theme.colors?.primary} !important`,
      p: noBorder ? 0 : '1.25rem !important',
      '@media screen and (max-width: 48rem)': {
        p: noBorder ? 0 : '0.5rem !important',
        borderWidth: '0.5rem !important'
      }
    }
  };
};

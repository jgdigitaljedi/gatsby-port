import { ThemeUIStyleObject } from 'theme-ui';
import theme from '../gatsby-plugin-theme-ui';

export const linkContainerStyle: ThemeUIStyleObject = {
  my: 2,
  ml: 4,
  alignItems: 'center'
};

export const linkStyle: ThemeUIStyleObject = { ml: 2, fontSize: 3 };

export const linkIconStyle = (theme.colors?.primary || '#fff') as string;

export const paraMargin: ThemeUIStyleObject = { mt: 4 };

export const jobsContainerStyle: ThemeUIStyleObject = {
  mt: 4,
  alignItems: 'center',
  flexDirection: 'column'
};

export const paraMarginBottom: ThemeUIStyleObject = { mb: 3 };

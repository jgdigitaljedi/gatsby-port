import bottomOfCOnsoleSHelves from '../images/bottomFoConsolesShelves.jpg';
import boxesShelf1 from '../images/boxesShelf1.jpg';
import boxesShelf2 from '../images/boxesShelf2.jpg';
import cgf2022Day1 from '../images/cgf2022Dat1.jpg';
import controllers1 from '../images/controllers1.jpg';
import crt from '../images/crt.jpg';
import topOfConsolesShelves from '../images/topOfConsolesShelves.jpg';

export const vgGalleryPics = [
  {
    image: bottomOfCOnsoleSHelves,
    title: 'Bottom area of console shelves'
  },
  {
    image: boxesShelf1,
    title: 'Shelf of console and game boxes 1'
  },
  {
    image: boxesShelf2,
    title: 'Shelf of console and game boxes 2'
  },
  {
    image: cgf2022Day1,
    title: 'Classic Game Fest 2022 day 1 haul'
  },
  {
    image: controllers1,
    title: 'N64 games drawer and some controllers'
  },
  {
    image: crt,
    title: '36 inch Sony Trinitron CRT TV'
  },
  {
    image: topOfConsolesShelves,
    title: 'Top area of console shelves'
  }
];

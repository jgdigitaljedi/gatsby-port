import { format, parseISO } from 'date-fns';

export const formatDate = (date: string | undefined): string => {
  return format(parseISO(date || ''), 'MMMM dd, yyyy');
};

const dev = process.env.NODE_ENV !== 'production';
export const server = dev ? 'http://localhost:3000' : 'https://joeyg.me';
